export const getStaticPaths = async () => {
  return {
    paths: [
      {
        params: {
          testId: "path with spaces"
        }
      }
    ],
    fallback: false
  };
};

export const getStaticProps = async () => {
  return {
    props: {}
  };
};

export default () => {
  return <div>path with spaces</div>;
};
